﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Scribot.Models;

namespace Scribot.DAL
{
    public class SchoolInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ScribotContext>
    {
        protected override void Seed(ScribotContext context)
        {
        }
    }
}