﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Hangfire;
using System.Configuration;
using System.Web.Http;
using Swashbuckle;
[assembly: OwinStartup(typeof(Scribot.NewsProcessor.Startup))]

namespace Scribot.NewsProcessor
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            Hangfire.GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["ScribotContext"].ConnectionString);
                        app.UseHangfireDashboard();
                        app.UseHangfireServer();

            ArticleProcessor processor = new ArticleProcessor();
            BackgroundJob.Enqueue(() => processor.ProcessNews());
          //TODO Add after tests  RecurringJob.AddOrUpdate(() => processor.ProcessNews(), Cron.Minutely());

        }
    }
}
