﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scribot.NewsProcessor.Models
{
    public class Rootobject
    {
        public string status { get; set; }
        public string source { get; set; }
        public string sortBy { get; set; }
        public Article[] articles { get; set; }
    }
}