﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scribot.NewsProcessor.Models
{
    public class Entry
    {
        public int EntryID { get; set; }
        public int ArticleID { get; set; }
        public string CompanyName { get; set; }
        public DateTime EntryDate { get; set; }
        public double Sentiment { get; set; }
        public int Category { get; set;} //0- undefined, 1 < 3 neutral >5
        public string KeyWords { get; set; }
        public virtual Article Article { get; set; }
    }
}