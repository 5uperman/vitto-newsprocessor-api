﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scribot.NewsProcessor.Models
{
    public class Article
    {
            public int ArticleID { get; set; }
            public string author { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string url { get; set; }
            public string urlToImage { get; set; }
            public DateTime publishedAt { get; set; }

        public virtual ICollection<Entry> Entries { get; set; }

    }

}
