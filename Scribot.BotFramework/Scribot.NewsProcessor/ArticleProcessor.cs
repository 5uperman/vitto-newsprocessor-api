﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using System.Net.Http;
using Scribot.NewsProcessor.Models;
namespace Scribot.NewsProcessor
{
    public class ArticleProcessor
    {
        private static string api_key = "&sortBy=latest&apiKey=d358be1cc2d24d939d5795da32fb64e7";
        private static string newsApi_uri = "https://newsapi.org/v1/articles?source=";
        private ScribotContext db = new ScribotContext();

        private static List<string> NewsSources = new List<string>(){ "techcrunch","business-insider-uk","engadget","mashable","the-economist"};
        private static List<string> Companies = new List<string>() { "Samsung", "Microsoft", "Apple", "Google" };
        //TODO Add other news sources and companies
        public  void ProcessNews()
        {
            List<Article> currentArticles = new List<Article>();
            foreach (string source in NewsSources)
            {
                Rootobject test = Newtonsoft.Json.JsonConvert.DeserializeObject<Rootobject>(ProcessSource(source).Result);
                int count = test.articles.Count();
                for (int i = 0; i <count; i++)
                {
                    Article temp = test.articles[i];
                    currentArticles.Add(temp);
                    //TODO CHECK for DUPLICATES
                    db.Articles.Add(temp);
                    db.SaveChanges();
                }
            }
            foreach (Article article in currentArticles)
            {
                List<Entry> processedEntries = ProcessArticle(article);
                db.Entries.AddRange(processedEntries);
                db.SaveChanges();
            }
        }
        public List<Entry> ProcessArticle(Article article)
        {
            List<Entry> processedEntries = new List<Entry>();
            string desc = article.description;
            double sentimentScore = TextAnalyticsHelper.GetSentiment(desc).Result.FirstOrDefault();

            int _category = 0;
            if (sentimentScore < 0.25) _category = 1;
            else if (sentimentScore < 0.5) _category = 2;
            else if (sentimentScore < 0.75) _category = 3;
            else _category = 4;

            List<string> keywords = TextAnalyticsHelper.GetKeyPhrases(desc).Result;

            var Companyquery = Companies
                .Where(p => keywords.Any(p2 => p2.Equals(p)))
                .ToList();

            foreach (string company in Companyquery)
            {
                Entry newEntry = new Entry()
                {
                    ArticleID = article.ArticleID,
                    CompanyName = company,
                    EntryDate = DateTime.Now,
                    Sentiment = sentimentScore,
                    Category= _category,
                    KeyWords = String.Join(";", keywords),
                    Article = article
                };
                processedEntries.Add(newEntry);
            }

            return processedEntries;
        }
        public async Task<String> ProcessSource(string source)
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(newsApi_uri+source+api_key);
            var result = await response.Content.ReadAsStringAsync();
            return result;
        }



    }
}