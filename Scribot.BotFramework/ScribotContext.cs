﻿using ScribotContext.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Scribot.DAL
{
    public class ScribotContext : DbContext
    {

        public ScribotContext() : base("ScribotContext")
        {
        }

        public DbSet<Entry> Entries { get; set; }
        public DbSet<Article> Articles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}